﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

namespace WebUsageLoggingService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://datamonitor.com/webservices/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UsageLogging : System.Web.Services.WebService
    {
        const string SMTP_SERVER = "SMTP_SERVER";
        const string MAIL_FROM_ADDRESS = "MAIL_FROM_ADDRESS";
        const string MAIL_TO_ADDRESS = "MAIL_TO_ADDRESS";

        [WebMethod]
        public string LogUsage(Nullable<Guid> User_Id, Nullable<Guid> Contract_Id, String Site, Decimal Value, String Additional_Details,
            String Product_Id, String IP_Address, String View_Code, String User_Type, String Event,
            String CalculationDetail, List<Int32> AvailableServices, List<Int64> DimVals, Nullable<Guid> SessionId, Nullable<DateTime> UserLocalTime)
        {
            try
            {
                if (null == Site){Site = string.Empty;}
                if (null == Value){Value = 0;}
                if (null == Additional_Details){Additional_Details = string.Empty;}
                if (null == Product_Id) { Product_Id = string.Empty; }
                if (null == IP_Address) { IP_Address = string.Empty; }
                if (null == View_Code) { View_Code = string.Empty; }
                if (null == User_Type) { User_Type = string.Empty; }
                if (null == Event) { Event = string.Empty; }
                if (null == CalculationDetail) { CalculationDetail = string.Empty; }
                if (null == AvailableServices){AvailableServices = new List<Int32>();}
                if (null == DimVals){DimVals = new List<Int64>();}
                DataTable tAvailableServices = CreateIntCollectionDataTableFromListOfInt32(AvailableServices);
                DataTable tDimVals = CreateBigintCollectionDataTableFromListOfInt64(DimVals);
                using (SqlConnection c = new SqlConnection(WebUsageLoggingConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.InsUsageLog", c))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter pUserId = cmd.Parameters.Add("@User_Id", SqlDbType.UniqueIdentifier);
                        SqlParameter pContractId = cmd.Parameters.Add("@Contract_Id", SqlDbType.UniqueIdentifier);
                        SqlParameter pSite = cmd.Parameters.Add("@Site", SqlDbType.VarChar, 50);
                        SqlParameter pValue = cmd.Parameters.Add("@Value", SqlDbType.Money);
                        SqlParameter pAdditionalDetails = cmd.Parameters.Add("@Additional_Details", SqlDbType.VarChar, 255);
                        SqlParameter pProductId = cmd.Parameters.Add("@Product_Id", SqlDbType.NVarChar, 65);
                        SqlParameter pIpAddress = cmd.Parameters.Add("@IP_Address", SqlDbType.VarChar, 15);
                        SqlParameter pViewCode = cmd.Parameters.Add("@View_Code", SqlDbType.VarChar, 255);
                        SqlParameter pUserType = cmd.Parameters.Add("@User_Type", SqlDbType.VarChar, 50);
                        SqlParameter pEvent = cmd.Parameters.Add("@Event", SqlDbType.VarChar, 255);
                        SqlParameter pCalculationDetail = cmd.Parameters.Add("@CalculationDetail", SqlDbType.Xml);
                        SqlParameter pAvailableServices = cmd.Parameters.Add("@AvailableServices", SqlDbType.Structured);
                        SqlParameter pDimVals = cmd.Parameters.Add("@DimVals", SqlDbType.Structured);
                        SqlParameter pSessionId = cmd.Parameters.Add("@SessionId", SqlDbType.UniqueIdentifier);
                        SqlParameter pUserLocalTime = cmd.Parameters.Add("@UserLocalTime", SqlDbType.DateTime);
                        if (null != User_Id)
                        {
                            pUserId.Value = User_Id;
                        }
                        if (null != Contract_Id)
                        {
                            pContractId.Value = Contract_Id;
                        }
                        pSite.Value = Site;
                        pValue.Value = Value; /*sic*/
                        if (0 < Additional_Details.Trim().Length)
                        {
                            pAdditionalDetails.Value = Additional_Details;
                        }
                        if (0 < Product_Id.Trim().Length)
                        {
                            pProductId.Value = Product_Id;
                        }
                        pIpAddress.Value = IP_Address;
                        if (0 < View_Code.Trim().Length)
                        {
                            pViewCode.Value = View_Code;
                        }
                        if (0 < User_Type.Trim().Length)
                        {
                            pUserType.Value = User_Type;
                        }
                        pEvent.Value = Event;
                        if (null != CalculationDetail)
                        {
                            pCalculationDetail.Value = CalculationDetail;
                        }
                        pAvailableServices.Value = tAvailableServices;
                        pDimVals.Value = tDimVals;
                        if (null != SessionId)
                        {
                            pSessionId.Value = SessionId;
                        }
                        if (null != UserLocalTime)
                        {
                            pUserLocalTime.Value = UserLocalTime;
                        }
                        c.Open();
                        cmd.ExecuteNonQuery();
                        return "Usage logged successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw ex;
            }
        }
        private string GetAppSetting(string Key)
        {
            return System.Web.Configuration.WebConfigurationManager.AppSettings[Key];
        }
        private void LogError(Exception ex)
        {
            System.Net.Mail.SmtpClient c = new System.Net.Mail.SmtpClient(GetAppSetting(SMTP_SERVER));
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(GetAppSetting(MAIL_FROM_ADDRESS), GetAppSetting(MAIL_TO_ADDRESS));
            msg.Body = ex.ToString();
            msg.Subject = "Error in WebUsageLoggingService.UsageLogging";
            msg.IsBodyHtml = false;
            c.Send(msg);
        }
        private String WebUsageLoggingConnectionString
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.ConnectionStrings["WebUsageLogging"].ConnectionString;
            }
        }
        private DataTable CreateIntCollectionDataTableFromListOfInt32(List<Int32> lst)
        {
            DataTable t = new DataTable("IntCollection");
            t.Columns.Add("IntCol", typeof(System.Int32));
            foreach (Int32 i in lst)
            {
                t.Rows.Add(i);
            }
            return t;
        }

        private DataTable CreateBigintCollectionDataTableFromListOfInt64(List<Int64> lst)
        {
            DataTable t = new DataTable("IntCollection");
            t.Columns.Add("IntCol", typeof(System.Int64));
            foreach (Int64 i in lst)
            {
                t.Rows.Add(i);
            }
            return t;
        }

        private void InitializeComponent()
        {

        }
    }
}
