﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

namespace WebUsageLoggingService
{
    /// <summary>
    /// Summary description for OmnitureUsageLogging
    /// </summary>
    [WebService(Namespace = "http://datamonitor.com/webservices/", Description = "Saves a copy of the data sent to Omniture during usage logging")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class OmnitureUsageLogging : System.Web.Services.WebService
    {
        const string SMTP_SERVER = "SMTP_SERVER";
        const string MAIL_FROM_ADDRESS = "MAIL_FROM_ADDRESS";
        const string MAIL_TO_ADDRESS = "MAIL_TO_ADDRESS";

        /// <summary>
        /// Saves a copy of the data we send to Omniture for internal analytics
        /// </summary>
        /// <param name="OmnitureData">The data we send to omniture as plain text</param>
        /// <param name="SystemType">System type parameter has 4 options: DEV, STAGE, LIVE, TEST. The value passed must be one of these.</param>
        /// <returns>Expect to get back "Usage logged successfully". If there is a failure, a System.Exception will be thrown.</returns>
        [WebMethod(Description="Saves a copy of the data sent to Omniture during usage logging for use in internal analytics. There are two parameters: OmnitureData is the plain text of the data sent to Omniture, in key-value pairs; SystemType describes the role of the system doing the logging, and must be one of DEV,TEST,STAGE or LIVE.")]
        public string LogUsage(string OmnitureData, string SystemType)
        {
            string rv = string.Empty;
            using (SqlConnection c = new SqlConnection(WebUsageLoggingConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.LogOmnitureUsage", c))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        cmd.Parameters.AddWithValue("@OmnitureUsageData", OmnitureData);
                        cmd.Parameters.AddWithValue("@SystemType", SystemType);
                        c.Open();
                        cmd.ExecuteNonQuery();
                        return "Usage logged successfully";
                    }
                    catch (Exception ex)
                    {
                        LogError(ex,SystemType,OmnitureData);
                        throw ex;
                    }
                }
            }
        }
         private string GetAppSetting(string Key)
        {
            return System.Web.Configuration.WebConfigurationManager.AppSettings[Key];
        }
        private void LogError(Exception ex, string SystemType, string OmnitureData)
        {
            System.Net.Mail.SmtpClient c = new System.Net.Mail.SmtpClient(GetAppSetting(SMTP_SERVER));
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(GetAppSetting(MAIL_FROM_ADDRESS), GetAppSetting(MAIL_TO_ADDRESS));
            System.Text.StringBuilder bldr = new System.Text.StringBuilder(ex.ToString());
            bldr.AppendLine();
            bldr.AppendLine("System type*************************");
            bldr.AppendLine(SystemType);
            bldr.AppendLine("Omniture data************************");
            bldr.AppendLine(OmnitureData);
            msg.Body = bldr.ToString();
            msg.Subject = "Error in WebUsageLoggingService.UsageLogging";
            msg.IsBodyHtml = false;
            c.Send(msg);
        }
        private String WebUsageLoggingConnectionString
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.ConnectionStrings["WebUsageLogging"].ConnectionString;
            }
        }
    }
}
