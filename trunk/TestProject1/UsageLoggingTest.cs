﻿using WebUsageLoggingService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Collections.Generic;

namespace TestProject1
{
    
    
    /// <summary>
    ///This is a test class for UsageLoggingTest and is intended
    ///to contain all UsageLoggingTest Unit Tests
    ///</summary>
    [TestClass()]
    public class UsageLoggingTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test that the service executes correctly with all parameters passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost:4840/BlankPageForUnitTests.aspx")]
        public void AllParamsPassed()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid();
            Nullable<Guid> Contract_Id = new Guid();
            string Site = "My Site";
            Decimal Value = 1234.56M;
            string Additional_Details = "Additional details";
            string Product_Id = "XX00001-001";
            string IP_Address = "127.0.0.1";
            string View_Code = "View code";
            string User_Type = "User type";
            string Event = "Event";
            string CalculationDetail = "<test description=\"All parameters provided correctly\"/>";
            Nullable<Guid> SessionIdParam = new Guid();
            Nullable<DateTime> UserLocalTime = DateTime.Now;
            List<int> AvailableServices = new List<Int32>(new Int32[] { 1, 2, 3 });
            List<Int64> DimVals = new List<Int64>(new Int64[] { 4, 5, 6 });
            string expected = "Usage logged successfully";
            string actual;
            actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals,SessionIdParam,UserLocalTime);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Test that the service throws an error if both user_id and contract_id are null
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost:4840/BlankPageForUnitTests.aspx")]
        public void UserAndContractBothNull()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = null;
            Nullable<Guid> Contract_Id = null;
            string Site = "My Site";
            Decimal Value = 1234.56M;
            string Additional_Details = "Additional details";
            string Product_Id = "XX00001-001";
            string IP_Address = "127.0.0.1";
            string View_Code = "View code";
            string User_Type = "User type";
            string Event = "Event";
            string CalculationDetail = "<test description=\"user id and contract id are both null - this should not appear in the table\"/>";
            List<int> AvailableServices = new List<Int32>(new Int32[] { 1, 2, 3 });
            List<Int64> DimVals = new List<Int64>(new Int64[] { 4, 5, 6 });
            string expected = "The INSERT statement conflicted with the CHECK constraint \"CHK_User_and_Contract_not_both_NULL\"";
            string actual;
            try
            {
                actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals, null, null);
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            if (actual.Contains("The INSERT statement conflicted with the CHECK constraint \"CHK_User_and_Contract_not_both_NULL\""))
            {
                actual = "The INSERT statement conflicted with the CHECK constraint \"CHK_User_and_Contract_not_both_NULL\"";
            }
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Test that the service throws an error if calculation detail is not valid xml
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void CalculationDetailContainsInvalidXml()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid();
            Nullable<Guid> Contract_Id = null;
            string Site = "My Site";
            Decimal Value = 1234.56M;
            string Additional_Details = "Additional details";
            string Product_Id = "XX00001-001";
            string IP_Address = "127.0.0.1";
            string View_Code = "View code";
            string User_Type = "User type";
            string Event = "Event";
            string CalculationDetail = "<test description=\"invalid calculation detail xml - this should not appear in the table\"";
            List<int> AvailableServices = new List<Int32>(new Int32[] { 1, 2, 3 });
            List<Int64> DimVals = new List<Int64>(new Int64[] { 4, 5, 6 });
            string expected = "XML parsing";
            string actual;
            try
            {
                actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals, null, null);
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            if (actual.Contains("XML parsing"))
            {
                actual = "XML parsing";
            }
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Test that the service completes without error if available services and dim vals are empty lists
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void AvailableServicesAndDimValsAreEmptyLists()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid();
            Nullable<Guid> Contract_Id = null;
            string Site = "My Site";
            Decimal Value = 1234.56M;
            string Additional_Details = "Additional details";
            string Product_Id = "XX00001-001";
            string IP_Address = "127.0.0.1";
            string View_Code = "View code";
            string User_Type = "User type";
            string Event = "Event";
            string CalculationDetail = "<test description=\"AvailableServices and DimVals are empty lists\"/>";
            List<int> AvailableServices = new List<Int32>();
            List<Int64> DimVals = new List<Int64>();
            string expected = "Usage logged successfully";
            string actual;
            actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals,null,null);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Test that the service completes without error if empty strings are passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost:4840/BlankPageForUnitTests.aspx")]
        public void NullStringsNotRecorded()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid();
            Nullable<Guid> Contract_Id = null;
            string Site = "My Site";
            Decimal Value = 1234.56M;
            string Additional_Details = String.Empty;
            string Product_Id = String.Empty;
            string IP_Address = "127.0.0.1";
            string View_Code = String.Empty;
            string User_Type = String.Empty;
            string Event = "Event";
            string CalculationDetail = String.Empty;
            List<int> AvailableServices = new List<Int32>();
            List<Int64> DimVals = new List<Int64>();
            Nullable<Guid> SessionId = new Guid();
            string expected = "Usage logged successfully";
            string actual;
            actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals,SessionId,null);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Test that the service completes without error if over-sized strings are passed (the strings should be truncated)
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void OversizedStrings()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid();
            Nullable<Guid> Contract_Id = null;
            string Site = new String('X', 100);
            Decimal Value = 1234.56M;
            string Additional_Details = String.Empty;
            string Product_Id = String.Empty;
            string IP_Address = "127.0.0.1";
            string View_Code = String.Empty;
            string User_Type = String.Empty;
            string Event = "Event";
            string CalculationDetail = String.Empty;
            List<int> AvailableServices = new List<Int32>();
            List<Int64> DimVals = new List<Int64>();
            string expected = "Usage logged successfully";
            string actual;
            actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals,null,null);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Test that the service throws an error if an invalid IP address is passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void InvalidIPAddress()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid();
            Nullable<Guid> Contract_Id = null;
            string Site = new String('X', 100);
            Decimal Value = 1234.56M;
            string Additional_Details = String.Empty;
            string Product_Id = String.Empty;
            string IP_Address = "IP ADDRESS";
            string View_Code = String.Empty;
            string User_Type = String.Empty;
            string Event = "Event";
            string CalculationDetail = String.Empty;
            List<int> AvailableServices = new List<Int32>();
            List<Int64> DimVals = new List<Int64>();
            string expected = "The INSERT statement conflicted with the CHECK constraint \"CHK_IpAddress_is_valid\"";
            string actual;
            try
            {
                actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals, null, null);
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            if (actual.Contains("The INSERT statement conflicted with the CHECK constraint \"CHK_IpAddress_is_valid\""))
            {
                actual = "The INSERT statement conflicted with the CHECK constraint \"CHK_IpAddress_is_valid\"";
            }
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test that the service accepts values greater than Int32.MaxValue for dim vals
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void BigIntDimVals()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid();
            Nullable<Guid> Contract_Id = null;
            string Site = new String('X', 100);
            Decimal Value = 1234.56M;
            string Additional_Details = String.Empty;
            string Product_Id = String.Empty;
            string IP_Address = "127.0.0.1";
            string View_Code = String.Empty;
            string User_Type = String.Empty;
            string Event = "Event";
            string CalculationDetail = String.Empty;
            List<int> AvailableServices = new List<Int32>();
            List<Int64> DimVals = new List<Int64>();
            Nullable<Guid> SessionId = new Guid();
            Nullable<DateTime> UserLocalTime = DateTime.Now;
            DimVals.Add(9223372036854775807); //this is the max value for bigint
            string expected = "Usage logged successfully";
            string actual;
            actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals,SessionId,UserLocalTime);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test for arbitrary values
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void ArbitraryData()
        {
            UsageLogging target = new UsageLogging(); // TODO: Initialize to an appropriate value
            Nullable<Guid> User_Id = new Guid("{CD02F2C5-A4A4-4246-B718-CEDCD140ED5B}");
            Nullable<Guid> Contract_Id = new Guid("{9AD98B74-D69F-432D-B73F-13E287C6E72C}");
            string Site = "Automotive Knowledge Center";
            Decimal Value = 125.000000M;
            string Additional_Details = "Full Company Profile View";
            string Product_Id ="63156298-A1B2-414F-81FC-74DF31BCF4C8";
            string IP_Address = "127.0.0.1";
            string View_Code = String.Empty;
            string User_Type = String.Empty;
            string Event = "CompanyProfile";
            string CalculationDetail = String.Empty;
            List<int> AvailableServices = null;
            List<Int64> DimVals = null;
            Nullable<Guid> SessionId = null;
            Nullable<DateTime> UserLocalTime = null;
            string expected = "Usage logged successfully";
            string actual;
            actual = target.LogUsage(User_Id, Contract_Id, Site, Value, Additional_Details, Product_Id, IP_Address, View_Code, User_Type, Event, CalculationDetail, AvailableServices, DimVals, SessionId, UserLocalTime);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test for data within expected boundaries
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void OmnitureExpectedData()
        {
            OmnitureUsageLogging target = new OmnitureUsageLogging(); // TODO: Initialize to an appropriate value
            string expected = "Usage logged successfully";
            string actual;
            string OmnitureData = "Test data within expected boundaries";
            string SystemType = "DEV";
            actual = target.LogUsage(OmnitureData,SystemType);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test for no data passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void OmnitureNullData()
        {
            OmnitureUsageLogging target = new OmnitureUsageLogging(); // TODO: Initialize to an appropriate value
            string expected = "ParamNotSupplied";
            string actual;
            try
            {
                actual = target.LogUsage(null,"DEV");
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            if (actual.Contains("'LogOmnitureUsage' expects parameter '@OmnitureUsageData', which was not supplied"))
            {
                actual = "ParamNotSupplied";
            }
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test for blank data passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void OmnitureBlankData()
        {
            OmnitureUsageLogging target = new OmnitureUsageLogging(); // TODO: Initialize to an appropriate value
            string expected = "CHK_OmnitureData";
            string actual;
            try
            {
                actual = target.LogUsage(string.Empty,"DEV");
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            if (actual.Contains("CHK_OmnitureData"))
            {
                actual = "CHK_OmnitureData";
            }
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test for no system parameter passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void OmnitureNoSystemParameter()
        {
            OmnitureUsageLogging target = new OmnitureUsageLogging(); // TODO: Initialize to an appropriate value
            string expected = "@SystemType";
            string actual;
            try
            {
                actual = target.LogUsage("Expected data", null);
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            if (actual.Contains("'LogOmnitureUsage' expects parameter '@SystemType', which was not supplied"))
            {
                actual = "@SystemType";
            }
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test for invalid system parameter passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void OmnitureInvalidSystemParameter()
        {
            OmnitureUsageLogging target = new OmnitureUsageLogging(); // TODO: Initialize to an appropriate value
            string expected = "CHK_SystemType";
            string actual;
            try
            {
                actual = target.LogUsage("Expected data", "potato");
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            if (actual.Contains("CHK_SystemType"))
            {
                actual = "CHK_SystemType";
            }
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        ///Test for dodgy xml passed
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Visual Studio Projects\WebUsageLoggingService\WebUsageLoggingService", "/")]
        [UrlToTest("http://localhost/BlankPageForUnitTests.aspx")]
        public void OmnitureInvalidXmlData()
        {
            OmnitureUsageLogging target = new OmnitureUsageLogging(); // TODO: Initialize to an appropriate value
            string expected = "XML parsing: line 1, character 26, semicolon expected";
            string actual;
            try
            {
                actual = target.LogUsage("<rx user=\"anonymous&amphy\" siteid=\"informadatamonitorhealthcarecomprod\" url=\"https://service.datamonitorhealthcare.com/disease/\"><r><p n=\"page-name\">hkc:disease</p><ev n=\"site-sections\">hkc:disease</ev></r><r><p n=\"site-section\">therapyarea</p></r><r><p n=\"server\">service.datamonitorhealthcare.com</p></r><r><p n=\"subsection1\">hkc</p><ev n=\"subsection1\">hkc</ev><p n=\"subsection2\">hkc:disease</p><ev n=\"subsection2\">hkc:disease</ev></r></rx>", "DEV");
            }
            catch (Exception ex)
            {
                actual = ex.Message;
            }
            Assert.AreEqual(expected, actual);
        }
    }
}
