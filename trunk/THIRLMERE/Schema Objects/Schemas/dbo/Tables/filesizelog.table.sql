﻿CREATE TABLE [dbo].[filesizelog] (
    [databaseName] VARCHAR (100) NULL,
    [LogicalName]  VARCHAR (100) NULL,
    [PhysicalName] VARCHAR (100) NULL,
    [sizeInMB]     REAL          NULL
);

