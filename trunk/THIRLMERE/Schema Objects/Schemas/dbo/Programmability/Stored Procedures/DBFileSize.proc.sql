﻿create proc DBFileSize
as
begin

insert into filesizelog (databaseName,LogicalName,PhysicalName,sizeInMB)
SELECT DB_NAME(database_id) AS DatabaseName,
Name ,
Physical_Name, (size*8)/1024 SizeMB
FROM sys.master_files


end
