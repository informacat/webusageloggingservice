﻿CREATE TYPE [dbo].[BigintCollection] AS  TABLE (
    [BigintCol] BIGINT NOT NULL,
    PRIMARY KEY CLUSTERED ([BigintCol] ASC));

