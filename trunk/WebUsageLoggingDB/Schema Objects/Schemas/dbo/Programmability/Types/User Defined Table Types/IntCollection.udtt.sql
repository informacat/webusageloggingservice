﻿CREATE TYPE [dbo].[IntCollection] AS  TABLE (
    [IntCol] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([IntCol] ASC));

