﻿CREATE PROC dbo.LogOmnitureUsage(
	@OmnitureUsageData NVARCHAR(2000)
)
AS
BEGIN
SET NOCOUNT ON;
INSERT dbo.OmnitureUsageLog (OmnitureData) VALUES (@OmnitureUsageData);
END