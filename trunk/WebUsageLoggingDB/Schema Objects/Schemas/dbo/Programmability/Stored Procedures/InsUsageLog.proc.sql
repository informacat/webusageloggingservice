




CREATE PROC [dbo].[InsUsageLog]
(
	@User_Id UNIQUEIDENTIFIER = NULL,
	@Contract_Id UNIQUEIDENTIFIER = NULL,
	@Site VARCHAR(50),
	@Value MONEY,
	@Additional_Details VARCHAR(255) = NULL,
	@Product_Id NVARCHAR(65) = NULL,
	@IP_Address VARCHAR(15),
	@View_Code VARCHAR(255) = NULL,
	@User_Type VARCHAR(50) = NULL,
	@Event VARCHAR(255),
	@CalculationDetail XML = NULL,
	@AvailableServices dbo.IntCollection READONLY,
	@DimVals dbo.BigintCollection READONLY,
	@SessionId UNIQUEIDENTIFIER = NULL,
	@UserLocalTime DATETIME = NULL
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	BEGIN TRAN;
		/*insert into dbo.UsageLog table*/
		INSERT dbo.UsageLog 
			([User_Id],Contract_Id,[Site],Value,Additional_Details,Product_Id,
			IP_Address,View_Code,User_Type,[Event],CalculationDetail,SessionId,UserLocalTime)
		VALUES 
			(@User_Id,@Contract_Id,@Site,@Value,@Additional_Details,@Product_Id,
			@IP_Address,@View_Code,@User_Type,@Event,@CalculationDetail,@SessionId,@UserLocalTime);
		/*insert into available services and dim vals if required*/
		IF (EXISTS (SELECT * FROM @AvailableServices) OR EXISTS(SELECT * FROM @DimVals)) 
		BEGIN
			/*get the new id*/
			DECLARE @Id BIGINT;
			SET @Id = SCOPE_IDENTITY();
			/*insert into available services*/
			IF EXISTS (SELECT * FROM @AvailableServices)
			BEGIN
				INSERT dbo.UsageAvailableServices (UsageLogId,ServiceId)
				SELECT @Id, IntCol
				FROM @AvailableServices;
			END
			/*insert into dimvals*/
			IF EXISTS (SELECT * FROM @DimVals)
			BEGIN
				INSERT dbo.UsageDimVals (UsageLogId,DimVal)
				SELECT @Id, BigintCol
				FROM @DimVals;
			END
		END
	COMMIT TRAN;
END





