﻿CREATE FUNCTION dbo.ValidateIpAddress
(
	@IpAddress VARCHAR(15)
)
RETURNS BIT
AS
BEGIN

/*Re-factor string into a table of char(1)*/
DECLARE @chars TABLE (C CHAR(1));
DECLARE @csr TINYINT;
SET @csr = 1;
WHILE @csr <= LEN(@IpAddress)
BEGIN
	INSERT @chars (C) VALUES (SUBSTRING(@IpAddress,@csr,1));
	SET @csr = @csr + 1;
END
/*Ensure we only have numeric characters and periods*/
IF EXISTS (SELECT * FROM @chars WHERE C NOT IN ('0','1','2','3','4','5','6','7','8','9','.'))
BEGIN
	RETURN 0;
END
/*Ensure that there are exactly 3 periods in the string*/
DECLARE @periodcount TINYINT;
SELECT @periodcount = COUNT(*) FROM @chars WHERE C = '.';
IF @periodcount <> 3
BEGIN
	RETURN 0;
END
	
/*(mis)use PARSENAME to separate IP Address into its 4 constituent parts*/
DECLARE @ip1 DECIMAL(12,0);
DECLARE @ip2 DECIMAL(12,0);
DECLARE @ip3 DECIMAL(12,0);
DECLARE @ip4 DECIMAL(12,0);

SET @ip4 = PARSENAME(@IpAddress,4);
SET @ip3 = PARSENAME(@IpAddress,3);
SET @ip2 = PARSENAME(@IpAddress,2);
SET @ip1 = PARSENAME(@IpAddress,1);

/*check that all sections are populated*/
IF (@ip1 IS NULL OR @ip2 IS NULL OR @ip3 IS NULL OR @ip4 IS NULL)
BEGIN
	RETURN 0;
END

/*check that each number is in the range 0-255*/
IF NOT ((@ip1 BETWEEN 0 AND 255) AND (@ip2 BETWEEN 0 AND 255) AND (@ip3 BETWEEN 0 AND 255) AND (@ip4 BETWEEN 0 AND 255))
BEGIN
	RETURN 0;
END
/*if we've got this far, the address is valid*/
RETURN 1;

END