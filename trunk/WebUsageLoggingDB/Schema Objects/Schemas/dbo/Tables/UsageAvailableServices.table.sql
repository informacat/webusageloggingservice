﻿CREATE TABLE [dbo].[UsageAvailableServices] (
    [Id]         BIGINT IDENTITY (1, 1) NOT NULL,
    [UsageLogId] BIGINT NOT NULL,
    [ServiceId]  INT    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 10, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
    FOREIGN KEY ([UsageLogId]) REFERENCES [dbo].[UsageLog] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION
);

