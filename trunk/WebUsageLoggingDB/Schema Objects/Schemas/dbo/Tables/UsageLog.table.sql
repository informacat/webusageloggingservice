CREATE TABLE [dbo].[UsageLog] (
    [Id]                 BIGINT           IDENTITY (1, 1) NOT NULL,
    [User_Id]            UNIQUEIDENTIFIER NULL,
    [Contract_Id]        UNIQUEIDENTIFIER NULL,
    [Site]               VARCHAR (50)     NOT NULL,
    [Value]              MONEY            NOT NULL,
    [Additional_Details] VARCHAR (255)    NULL,
    [Product_Id]         NVARCHAR (65)    NULL,
    [Viewed_Date]        DATETIME         DEFAULT (getutcdate()) NOT NULL,
    [IP_Address]         VARCHAR (15)     NOT NULL,
    [View_Code]          VARCHAR (255)    NULL,
    [User_Type]          VARCHAR (50)     NULL,
    [Event]              VARCHAR (255)    NOT NULL,
    [CalculationDetail]  XML              NULL,
    [SessionId]          UNIQUEIDENTIFIER NULL,
    [UserLocalTime]      DATETIME         NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 10, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);



