﻿ALTER TABLE [dbo].[UsageLog]
    ADD CONSTRAINT [CHK_User_and_Contract_not_both_NULL] CHECK ([User_Id] IS NOT NULL OR [Contract_Id] IS NOT NULL);

